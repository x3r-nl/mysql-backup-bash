#!/bin/bash

source config.sh

clear
echo $'########################################\n#\n# MySQL Import tool\n# RvStFyth\n#\n########################################\n\n\n'

read -p 'Username: ' username
while [ -z $username ]; do
	echo -e "${yellow}Enter a username${default}"
	read -p 'Username: ' username
done

read -sp 'Password: ' password

read -p $'\nHost: ' host
if [ -z $host ]; then
	host="127.0.0.1"
	echo "host defaulted to 127.0.0.1"
fi

read -p 'Name of the new database: ' db_name
while [ -z $db_name ]; do
	echo -e "${yellow}Database name is required${default}"
	read -p 'Name of the new database: ' db_name
done

read -p 'File location: ' import_file

while [[ -z $import_file || ! -f $import_file.tar.gz ]]; do 
	echo -e "${yellow}Enter a valid file to import${default}"
	read -p 'File location: ' import_file
done

tar zxvf $import_file.tar.gz $import_file.sql >> log.log

# DROP IF EXISTS, CREATE AND USE $db_name
mysql -u $username -p$password -e "DROP DATABASE IF EXISTS $db_name; CREATE DATABASE $db_name /*\!40100 DEFAULT CHARACTER SET utf8 */; USE $db_name" >> log.log
mysql -u $username -p$password $db_name < $import_file.sql >> log.log

rm $import_file.sql

echo -e "${green}Import completed${default}"
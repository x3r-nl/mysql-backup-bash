#!/bin/bash

source config.sh

declare -a notifications
if (( "$#" > 0 )); then
	suppress_output=true
	while getopts "u:p:h:d:" opt;  do
		case "${opt}" in
			u) user="${OPTARG}";;
			p) password="${OPTARG}";;
			h) host="${OPTARG}";;
			d) db_name="${OPTARG}";;
		esac
	done
	
	# Validate parameters
	if [ -z ${user+x} ]; then
		echo "No username supplied, stopping script" >> log.log
		exit 1
	fi

	if [ -z ${db_name+x} ]; then
		echo "No database name supplied, stopping script" >> log.log
		exit 1
	fi

	if [ -z ${host+x} -o -z $host ]; then
		host="127.0.0.1"
	fi

else
	suppress_output=false;
	clear
	echo $'########################################\n#\n# MySQL Backup tool\n# RvStFyth\n#\n########################################\n\n\n'

	read -p 'username: ' user
	while [ -z $user ]
	do
		echo -e "${yellow}Username is required${default}"
		read -p 'Username: ' user
	done 
	read -sp 'password: ' password
	read -p $'\nhost (leave empty for localhost): ' host

	if [ -z $host ]; then 
	    host="127.0.0.1" 
	    echo "host defaulted to 127.0.0.1"
	fi

	read -p 'Database name: ' db_name
	while [ -z $db_name ]
	do
		echo -e "${yellow}Database name is required${default}"
		read -p 'Database name: ' db_name
	done

	read -p 'Target path (leave empty for current directory: ' targetPath
	if [ ! -z $targetPath ]; then
		if [ -d "$targetPath" ]; then
			if [ -w "$targetPath" ]; then
				cd $targetPath
				if [ $? -eq 0 ]; then
					echo -e "${green}Changed directory to $targetPath ${default}"
				else
					echo -e "${yellow}Failed to change directory, using current directory${default}"
				fi
			else
				echo -e "${yellow}Target path is not writable, check permissions. Using current directory${default}"
			fi
		else
			echo -e "${yellow}Path not valid, using current directory${default}"
		fi
	fi
fi

filename=$db_name-$(date +"%Y-%m-%d")

mysqldump --user=$user --password=$password --host=$host --add-drop-database $db_name > $filename.sql 2> /dev/null
if [[ $? -ne 0 ]]; then 
   	notifications+=("${red}Unable to create backup, check log file${default}")
    rm $filename.sql
else	
	notifications+=("${green}Created backup${default}")
    tar -cvzf $filename.tar.gz $filename.sql >> log.log
    if [[ $? -ne 0 ]]; then 
        notifications+=("${yellow}Unable to compress backup, check log file${default}")
        exit 1
    else
        notifications+=("Backup compressed: $filename.tar.gz")
        rm $filename.sql
        notifications+=("Deleted SQL file")
    fi
fi

for V in "${notifications[@]}"; do
	if [ "${suppress_output}" = true ]; then
		echo $V >> log.log
	else
		echo -e $V
	fi
done